#!/bin/zsh

# use 
# ./setupnpm.zsh -v 18
# to install node version 18
#
# use 
# ./setupnpm.zsh -v 18 -d
# to install node version 18 and set it as default
#
# to start using this script, use
# chmod 0755 ./setupnpm.zsh

VERSION='18'
SETDEFAULT="NO"

while getopts 'v:d' flag; do
  case "${flag}" in
    v) VERSION="${OPTARG}" ;;
    d) SETDEFAULT="YES" ;;
  esac
done

echo
echo "INSTALLING BREW"
CHECKBREW=$(which brew 2>&1)

BREWTYPE="NONE"

# INTEL BREW
if [[ $CHECKBREW == "/usr/local/bin/brew" ]]; then
    BREWTYPE="INTEL"
# APPLE SILICON BREW
elif [[ $CHECKBREW == "/opt/homebrew/bin/brew" ]]; then
    BREWTYPE="APPLE"
else
    /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
    CHECKBREW=$(which brew 2>&1)
    if [[ $CHECKBREW == "/usr/local/bin/brew" ]]; then
        BREWTYPE="INTEL"
    # APPLE SILICON BREW
    elif [[ $CHECKBREW == "/opt/homebrew/bin/brew" ]]; then
        BREWTYPE="APPLE"
    fi
fi

if [[ $BREWTYPE == "NONE" ]]; then
    echo "ERROR: unable to install brew"
    echo "------- SCRIPT ENDED -------"
    exit 1
fi

echo "+ Installed brew version: $BREWTYPE"

echo
echo "INSTALLING NVM"
NVMNOTFOUND="command not found"
CHECKNVM=$(nvm -v 2>&1)

if [[ "$CHECKNVM" == *"$NVMNOTFOUND"* ]]; then
    brew install nvm
    CHECKNVM=$(nvm -v 2>&1)
    if [[ "$CHECKNVM" == *"$NVMNOTFOUND"* ]]; then
        echo "ERROR: unable to install nvm"
        echo "------- SCRIPT ENDED -------"
        exit 1
    fi
fi

echo "+ Installed nvm version: $CHECKNVM"

echo
echo "INSTALLING NPM"
CHECKNPM=$(nvm list 2>&1)

if [[ "$CHECKNPM" == *"(-> v$VERSION"* ]]; then
    nvm use $VERSION
    CHECKNPM=$(node -v 2>&1)
    echo "+ Version $CHECKNPM has already been installed."
else
    nvm install $VERSION
    nvm use $VERSION
    CHECKNPM=$(node -v 2>&1)
    echo "+ Done installing npm $CHECKNPM"
    if [[ $SETDEFAULT == "YES" ]]; then
        nvm alias default $CHECKNPM
        echo "+ Set default to npm $CHECKNPM"
    fi
fi

echo
echo "----------------------------"
nvm list
echo "----------------------------"
echo

echo "------- SCRIPT ENDED -------"